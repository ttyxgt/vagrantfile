# vagrantfile
Vagrantfile template.
<br>
Create multi VMs and Netwarks.
VM is based on centos/7 box and default provisioner install ansible.
Each VMs can communicate each other on defined network (private or NAT network).
<br>
Some ansible playbook included.

## usage
edit networks and guests variables in Vagrantfile. and vagrant up.

### networks
networks is an array of hash. each hash define a network address(CDIR), network type (intnet or natnetwork) and network name.
like this.

    networks = [
      # keep eth0(nic1) default setting (nat/dhcp)
      {addr:'192.168.10.0/24', type:'natnetwork', name:'natnet10'}, #eth1(nic2)
      nil,#{addr:'192.168.20.0/24', type:'intnet',name:'intnet20'}  #eth2(nic3)
      nil #{addr:'192.168.30.0/24', type:'intnet',name:'intnet30'}  #eth3(nic4)
    ]

### guests
guests is an array of VMs to create. 

    guests = [
      {name:'vm1'},
      {name:'vm2', addr:['192.168.10.10/24'], cpus:2, mem:2048 },
    ]

You can specify bellow parameters for ehch VM.
- name:        required. host name and VirtualBox GUI name. default none.
- mem:         memory size. default 512.
- port[]:      array of forwarded ports. like `'web':[80,8000], 'ssl':[443,44300]`
- share_type:  shared dir(/vagrant) type. default 'virtualbox'.
- http_proxy:  vagrant-proxyconf setting. default ENV['http_proxy'] of host machine.
- https_proxy: vagrant-proxyconf setting. default ENV['https_proxy'] of hostmachine.
- no_proxy:    vagrant-proxyconf setting. default ENV['no_proxy'] of hostmachine.
- addr[]:      array of ip addresses of this VM. On multi network, Define VM address follow the order of networks array.
- provision[]: array of provision command. Specified commands will executed after default provisioner(see bellow) finished. 
- cpus:        number of CPU of the VM.

### multiconfig function
multiconfig function configure VMs and networks.

    $LOAD_PATH.push("./vagrantfiles")
    require "vgconfig"
    vgconfig(guests, networks)

## default provisioner

`./provision/default.sh` is executed when vagrant do provision.
- If files exist in ./provision/cacerts dir, Add CA certs files into /etc/pki/ca-trust/source/anchor and invoke update-ca-trust command.
- install ansible (and epel).
- generate root ssh key pair and set pubic key to authorized_keys to access localhost by ansible.
- copy root public key to vagrant shared folder.
- copy root public keys of other machines in vagrant shared folder to authorized_keys.

If ip addresses of VM are defined, `./provision/eth.sh` configure NICs of the VM and bring up it.

