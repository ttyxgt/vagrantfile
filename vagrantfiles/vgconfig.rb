# -*- mode: ruby -*-
# vi: set ft=ruby ts=2 shiftwidth=2 ff=unix:

def modifyvm(vb, opt, str)
  vb.customize ["modifyvm", :id, opt, str]
end
###
def vm_config(
  name:        , #required
  box:         'centos/7',
  mem:         512,
  share_type:  'virtualbox',
  http_proxy:  ENV['http_proxy'],
  https_proxy: ENV['https_proxy'],
  no_proxy:    ENV['no_proxy'],
  addr:        [],
  provision:   [],
  cpus:        1,
  port:        {}
)
  Vagrant.configure("2") do |c|
    c.vm.define name do |host|
      ### box
      host.vm.box = box
      ### hostname
      host.vm.hostname = name unless name.nil?
      ### forwarded ports
      port.each do |name, p|
        host.vm.network "forwarded_port", id:name, guest:p[0], host:p[1]
      end
      ### base provisioning 
      host.vm.provision("shell", inline: "/vagrant/provision/default.sh")
      ### nic provisioning
      addr.each.with_index(1) do |a, i|
        eth = "/vagrant/provision/eth.sh #{i} #{a}"
        host.vm.provision("shell", inline: eth)
      end
      ### custom provisioning
      unless provision.nil?
        provision.each do |p| 
          host.vm.provision("shell", inline: p)
        end
      end
      ### share type
      host.vm.synced_folder(
        ".", '/vagrant', type: share_type) unless share_type.nil?
      ### vbox settings
      host.vm.provider :virtualbox do |vb|
        # gui name
        vb.name = name unless name.nil?
        # memory size
        vb.memory = mem unless mem.nil?
        # num cpus
        if cpus != 1
          vb.cpus = cpus
          vb.customize ["modifyvm", :id, "--ioapic", "on"]
        end
      end
      ### http proxy
      unless http_proxy.nil? then
        http_proxy = "http://#{http_proxy}" unless http_proxy.include?("://") 
        host.proxy.http = http_proxy
      end
      ### https proxy
      unless https_proxy.nil? then
        https_proxy = 
          "https://#{https_proxy}" unless https_proxy.include?("://") 
        host.proxy.https = https_proxy
      end
      ### no_proxy
      host.proxy.no_proxy = no_proxy unless no_proxy.nil?
      ### vbguest/centos8
      c.vbguest.installer_options = { allow_kernel_upgrade: true }
    end
  end
end
###
def vgconfig ( guests, networks )
  # create networks
  Vagrant.configure("2") do |c|
    c.vm.provider :virtualbox do |vb| 
      networks.each.with_index(2) do |net, i|
        next if net.nil?
        next if net[:type].nil?
        next if net[:addr].nil?
        modifyvm(vb, "--nic"       +i.to_s, net[:type]) 
        modifyvm(vb, "--natnet"    +i.to_s, net[:addr]) 
        modifyvm(vb, "--nictype"   +i.to_s, "82540EM")
        modifyvm(vb, "--nicpromisc"+i.to_s, "allow-all")
        case net[:name]
        when 'natnetwork'
          modifyvm(vb, "--nat-network", i, net[:name])
        when 'intnet'
          modifyvm(vb, "--intnet"     , i, net[:name])
        end
      end
    end
  end

  # create vms
  guests.each do |guest|
    vm_config(guest)
  end
end
