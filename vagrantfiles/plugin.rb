# -*- mode: ruby -*-
# vi: set ft=ruby :

###
def plugin( p )
  system "vagrant plugin install #{p}" unless Vagrant.has_plugin? p
end
