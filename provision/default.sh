#! /bin/sh
# -*- mode: sh -*-
# vi: set ft=sh ts=2 shiftwidth=2 ff=unix:

[ -z "`grep 'CentOS Linux release 7' /etc/centos-release`" ] &&
  echo "This Default Provisioner is only for CentOS7" 1>&2 &&
  exit 0

if [ `id -u` -ne 0 ]; then
  echo '***** You are not the root.' 1>&2
  echo "***** sudo $*" 1>&2
  exec sudo $*
fi
PATH=/bin:/usr/bin
provisiondir=`dirname $0`

echo '======================='
echo 'install trusted ca cert'
echo '======================='
CERTS="${provisiondir}/cacerts"
CERTDIR=/etc/pki/ca-trust/source/anchors
CABUNDLE=/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
ls -l ${CERTS}
[ -d ${CERTS} ] && ls ${CERTS}/ | while read f
do
  [ ! -f ${CERTDIR}/$f ] &&
    echo "COPY $f" &&
    cp ${CERTS}/$f ${CERTDIR} &&
    ls -l ${CERTDIR}/$f
done
if [ ${CERTDIR} -nt ${CABUNDLE} ]; then
  echo 'UPDATE'
  update-ca-trust enable
  update-ca-trust extract
else
  echo 'SKIP'
fi
ls -l ${CABUNDLE}

echo '========================'
echo 'Add epel'
echo '========================'
yum install epel-release -y

echo '========================'
echo 'install ansible'
echo '========================'
yum install ansible -y
yum install libselinux-python -y

echo '========================'
echo 'generate root key to ssh localhost'
echo '========================'
sshdir=/root/.ssh
privkey=${sshdir}/id_rsa
pubkey=${sshdir}/id_rsa.pub
authkey=${sshdir}/authorized_keys
hosts=${sshdir}/known_hosts

if [ -f "${privkey}" ]; then
    echo 'SKIP'
else
  ssh-keygen -t rsa -b 4096 -f ${privkey} -P '' -C root@`uname -n` &&
  cat "${pubkey}" >> ${authkey} &&
  chmod 0600 ${authkey}
  ssh-keyscan localhost >> ${hosts}
fi
ls -l $privkey $pubkey $authkey $hosts

echo '========================'
echo 'copy root pubkey to shared foler'
echo '========================'
rootkey='rootkey.pub'
pub="/vagrant/.vagrant/machines/$(uname -n)/virtualbox/${rootkey}"
diff $pubkey $pub > /dev/null 2>&1
if [ "$?" -ne 0 ]; then
  echo COPY
  cp $pubkey $pub || exit 1
else
  echo 'SKIP'
fi
ls -l $pub

echo '========================'
echo 'add pubkeys from shared foler to authorized_keys'
echo '========================'
ls /vagrant/.vagrant/machines/*/virtualbox/${rootkey} |
while read path
do
  m="`echo $path | cut -d/ -f 5`"
  if [ -z "`grep -f ${path} ${authkey}`" ]; then
    echo ADD  $m &&
    cat "${path}" >> ${authkey} 
  else
    echo SKIP $m
  fi
done
ls -l ${authkey}
wc -l ${authkey}

echo '========================'
echo "end of provisioning: $0 $*"
echo '========================'
exit 0
