#! /bin/sh 
# -*- mode: sh -*-
# vi: set ft=sh ts=2 shiftwidth=2 ff=unix:

#! /bin/sh
if [ `id -u` -ne 0 ]; then
  echo '***** You are not the root.' 1>&2
  echo "***** sudo $*" 1>&2
  exec sudo $*
fi
PATH=/bin:/usr/bin
n=$1
addr=$2
[ -z "$n" -o -z "$addr" ] &&
	echo "$0: invalid options" 1>&2 &&
	exit 1
device="eth${n}"
ifname="Wired connection ${n}"

echo '======================='
echo "configure '${ifname}' -> $addr"
echo '======================='
nmcli connection modify "${ifname}" \
	connection.interface-name "${device}" \
	ipv4.addresses "${addr}" \
	ipv4.method manual \
	connection.autoconnect yes ||
  exit 1
### don't change connectionid. it cause troublesome when re-provision
###	connection.id "${device}" \

nmcli connection show

echo '======================='
echo "bring up '${ifname}'"
echo '======================='
nmcli connection up "${ifname}"
